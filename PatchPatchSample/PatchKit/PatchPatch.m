//
//  PatchPatch.m
//  PatchPatchSample
//
//  Created by John Kokkinidis on 7/3/13.
//  Copyright (c) 2013 John Kokkinidis. All rights reserved.
//

#import "PatchPatch.h"

@implementation PatchPatch

+ (BOOL)patchOldFile:(NSString*)oldFile
             newFile:(NSString*)newFile
               patch:(NSString*)patchFile {
    return bspatch([oldFile cStringUsingEncoding:NSUTF8StringEncoding],
                   [newFile cStringUsingEncoding:NSUTF8StringEncoding],
                   [patchFile cStringUsingEncoding:NSUTF8StringEncoding]);
}

+ (void)patchOldFile:(NSString*)oldFile
             newFile:(NSString*)newFile
               patch:(NSString*)patchFile
            delegate:(NSObject<GCBSPatchDelegate>*)delegate {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^(void) {
        if ([PatchPatch patchOldFile:oldFile newFile:newFile patch:patchFile]) {
            [delegate patchDidSucceed];
        } else {
            [delegate patchDidFail];
        }
    }];
}

@end
