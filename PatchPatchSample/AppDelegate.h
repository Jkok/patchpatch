//
//  AppDelegate.h
//  PatchPatchSample
//
//  Created by John Kokkinidis on 7/3/13.
//  Copyright (c) 2013 John Kokkinidis. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
