//
//  PatchPatch.h
//  PatchPatchSample
//
//  Created by John Kokkinidis on 7/3/13.
//  Copyright (c) 2013 John Kokkinidis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "bspatch.h"

@protocol GCBSPatchDelegate <NSObject>
- (void)patchDidSucceed;
- (void)patchDidFail;
@end


@interface PatchPatch : NSObject


+ (BOOL)patchOldFile:(NSString*)oldFile
             newFile:(NSString*)newFile
               patch:(NSString*)patchFile;

+ (void)patchOldFile:(NSString*)oldFile
             newFile:(NSString*)newFile
               patch:(NSString*)patchFile
            delegate:(NSObject<GCBSPatchDelegate>*)delegate;


@end
