//
//  ViewController.h
//  PatchPatchSample
//
//  Created by John Kokkinidis on 7/3/13.
//  Copyright (c) 2013 John Kokkinidis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PatchPatch.h"




#define OLD_FILE "oldFile.txt"
#define PATCH_FILE "validPatch.bsdiff"

#define NEW_FILE "newFile.txt"



@interface ViewController : UIViewController
- (IBAction)onClick:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *timeElapsedLabel;



@end
