//
//  ViewController.m
//  PatchPatchSample
//
//  Created by John Kokkinidis on 7/3/13.
//  Copyright (c) 2013 John Kokkinidis. All rights reserved.
//



#import "ViewController.h"

@interface ViewController ()
{
    //String paths of:
    
    NSString* oldFile;          //The file which we will be updating
    NSString* patchFile;        //What we will be updating oldFile with.
    NSString* newFile;          //The resulting (UPDATED) file.
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setFileNames];            //initing string paths
}




-(void) setFileNames
{
    /*
     
     Setting the String paths. Currently our files reside in the main bundle (Supporting files dir)
     
     We set the input file paths on string variables.
     Essentially we need 2 files.
     -The oldFile.txt (which will be updated) and
     -The validPatch.bsdiff (which we'll be updating the oldFile.txt with).
     */
    
    oldFile = [[NSString alloc] initWithBytes:OLD_FILE length:sizeof(OLD_FILE) encoding:NSASCIIStringEncoding];
    oldFile= [[NSBundle mainBundle]
              pathForResource:oldFile
              ofType:@""];
    
//    NSLog(@"Old file found at path:/n %@", oldFile);
    
    
    patchFile = [[NSString alloc] initWithBytes:PATCH_FILE length:sizeof(PATCH_FILE) encoding:NSASCIIStringEncoding];
    patchFile= [[NSBundle mainBundle]
                pathForResource:patchFile
                ofType:@""];
//    NSLog(@"Patch file found at path:/n %@", patchFile);
    
    
    /*
     
     Setting the string path of our new file (The UPDATED one - the one we will be creating)
     For ease of use we will be creating this file within our applications Documents directory (NSDocumentDirectory).
     */
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);   // Get documents folder
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    newFile = [[NSString alloc] initWithBytes:NEW_FILE length:sizeof(NEW_FILE) encoding:NSASCIIStringEncoding];
    newFile = [documentsDirectory stringByAppendingPathComponent:newFile];            //Specify the new files filename.
    NSLog(@"Updated (new) file found at path:/n %@", newFile);
    
}


- (IBAction)onClick:(id)sender {
    
    NSDate* startingTime = [[NSDate alloc] init];       //we keep the current time (in order to display time elapsed later on)
    
    
    

    /*
     Here is where the work is done. The function is self explainatory. It does mess with file input/output.
     When this line has completed, there WILL be a
     */
    [PatchPatch patchOldFile:oldFile newFile:newFile patch:patchFile];
    

    
    
    
    
    
    float timeElapsed = [startingTime timeIntervalSinceNow];        //we calculate the milliseconds that have passed
    
    [_timeElapsedLabel setText:[[NSString alloc] initWithFormat:@"%.2f seconds.",ABS(timeElapsed)*100 ]];   //set the label with the time that passed.
}




@end
